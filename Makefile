xmonad-x86_64-linux: xmonad-config.cabal Main.hs dist-newstyle/ Setup.hs
	cabal install xmonad-x86_64-linux --installdir=/home/jaro/.xmonad/ --overwrite-policy=always -j1

dist-newstyle/: xmonad-config.cabal Setup.hs
	cabal build --dependencies-only

clean:
	cabal clean
	rm -rf dist-newstyle/
	rm -f xmonad-x86_64-linux

module Main (main) where

import XMonad
import XMonad.Hooks.DynamicLog
import qualified Data.Map as M
import Graphics.X11.ExtraTypes.XF86
import XMonad.Layout.NoBorders (smartBorders)
import XMonad.Hooks.ManageHelpers (doFullFloat)
import Control.Monad ((>=>))
import Data.Monoid (appEndo)
import XMonad.Hooks.EwmhDesktops (ewmh)
import XMonad.Hooks.ManageDocks (docks, avoidStruts)
import XMonad.Hooks.SetWMName (setWMName)
import Control.Applicative (liftA2)
import System.IO (hPutStrLn)
import XMonad.Util.Run (spawnPipe)

main :: IO ()
main = do
  xmproc <- spawnPipe "/home/jaro/.cabal/bin/xmobar /home/jaro/.xmobarrc"
  xmonad . docks $ ewmh def
    { terminal    = "urxvt"
    , modMask     = mod4Mask
    , keys        = liftA2 M.union myKeys (keys def)
    , layoutHook  = avoidStruts (smartBorders (layoutHook def))
    , startupHook = setWMName "LG3D" *> spawn "autorandr --change"
    , logHook = dynamicLogWithPP xmobarPP
      { ppOutput = hPutStrLn xmproc
      , ppTitle = xmobarColor "green" "" . shorten 50
      }
    }

myKeys (XConfig {modMask = modm}) = M.fromList
  -- screensaver / lock
  [ ((modm, xK_x), spawn "xscreensaver-command -lock") -- xscreensaver must be running

  -- volume
  , ((noModMask, xF86XK_AudioLowerVolume), spawn "amixer sset Master 5%-")
  , ((noModMask, xF86XK_AudioRaiseVolume), spawn "amixer sset Master 5%+")
  , ((noModMask, xF86XK_AudioMute       ), spawn "amixer sset Master toggle")

  -- music
  , ((noModMask, xF86XK_AudioPlay), spawn "mpc toggle")
  , ((noModMask, xF86XK_AudioStop), spawn "mpc stop")
  , ((noModMask, xF86XK_AudioNext), spawn "mpc next")
  , ((noModMask, xF86XK_AudioPrev), spawn "mpc prev")

  -- backlight
  , ((noModMask, xF86XK_MonBrightnessUp  ), spawn "xbacklight -inc 10")
  , ((noModMask, xF86XK_MonBrightnessDown), spawn "xbacklight -dec 10")

  -- full screen
  , ((modm, xK_f), withFocused (runQuery doFullFloat >=> windows . appEndo))

  -- screenshot
  , ((noModMask, xK_Print  ), spawn "maim -s | xclip -selection clipboard -t image/png")
  , ((controlMask, xK_Print), spawn "maim    | xclip -selection clipboard -t image/png")

  -- yeganesh with dmenu
  , ((modm, xK_p), spawn "$(yeganesh -x)")

  ]
